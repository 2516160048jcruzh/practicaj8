package com.gft.practica.capitulos.cap2;

import com.gft.practica.models.Apple;

public class AppleGreenColorPredicate implements ApplePredicate {

	@Override
	public boolean test(Apple apple) {
		
		return "green".equals(apple.getColor());
	}

}
