package com.gft.practica.capitulos.cap2;

import java.util.ArrayList;
import java.util.List;

import com.gft.practica.models.Apple;

public class Seccion2_2 {

	public static void main(String[] args) {

		System.out.println("Iniciando ....");
		List<Apple> inventory = generateRandomApple(50);

		prettyPrintApple(inventory, new AppleFancyFormatter());
		List<Apple> greenApples = filterApplesByColor(inventory, "green");
		List<Apple> redApples = filterApplesByColor(inventory, "red");

		System.out.println("Manzanas verdes:" + greenApples.size());

		System.out.println("Manzanas rojas:" + redApples.size());
		
		
		
		List<Apple> redAndHeavyApples =
				filter(inventory, new AppleRedAndHeavyPredicate());
		

	}

	private static List<Apple> filter(List<Apple> inventory, ApplePredicate p) {
		List<Apple>result=new ArrayList<>();
		for(Apple apple:inventory) {
			if(p.test(apple)) {
				result.add(apple);
			}
		}
		return result;
	}

	private static List<Apple> generateRandomApple(int total) {

		List<Apple> inventoryRand = new ArrayList<Apple>();

		int cont = 0;

		while (cont < total) {
			Apple appleRand = new Apple();

			appleRand.setWeight(cont * 10);

			appleRand.setColor((cont * 1.5) % 2 == 0 ? "green" : "red");

			inventoryRand.add(appleRand);

			cont++;
		}

		return inventoryRand;

	}

	public static List<Apple> filterApplesByColor(List<Apple> inventory, String color) {
		List<Apple> result = new ArrayList<Apple>();
		for (Apple apple : inventory) {
			if (apple.getColor().equals(color)) {
				result.add(apple);
			}
		}
		return result;
	}

	
	public static List<Apple> filterApplesByWeight(List<Apple> inventory, int weight) {

		List<Apple> result = new ArrayList<>();
		for (Apple apple : inventory) {
			if (apple.getWeight() > weight) {
				result.add(apple);
			}
		}
		return result;
	}
	
	
	public static List<Apple> filterApplesUsingLambdas(List<Apple> inventory) {

		List<Apple> result =
				filter(inventory, (Apple apple) -> "red".equals(apple.getColor()));
		return result;
	}
	
	
	
	
	public static void prettyPrintApple(List<Apple> inventory,
			AppleFormatter formatter){
			for(Apple apple: inventory){
			String output = formatter.accept(apple);
			System.out.println(output);
			}
			}
	
	
	

}
