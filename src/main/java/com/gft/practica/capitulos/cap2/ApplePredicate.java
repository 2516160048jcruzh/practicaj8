package com.gft.practica.capitulos.cap2;

import com.gft.practica.models.Apple;

public interface ApplePredicate {
	
	boolean test (Apple apple);

}
