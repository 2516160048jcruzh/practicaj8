package com.gft.practica.capitulos.cap2;

import com.gft.practica.models.Apple;

public class AppleRedAndHeavyPredicate implements ApplePredicate {

	@Override
	public boolean test(Apple apple) {
		
	 return "red".equals(apple.getColor())
				&& apple.getWeight() > 150;
	}

}
