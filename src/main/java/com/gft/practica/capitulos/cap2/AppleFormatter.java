package com.gft.practica.capitulos.cap2;

import com.gft.practica.models.Apple;

public interface AppleFormatter {
	String accept(Apple a);
}
