package com.gft.practica.capitulos.cap2;

import com.gft.practica.models.Apple;

public class AppleHeavyWeightPredicate implements ApplePredicate {

	@Override
	public boolean test(Apple apple) {
		
		return apple.getWeight()>150;
	}

}
