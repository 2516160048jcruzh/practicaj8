package com.gft.practica.capitulos.cap3;

import java.util.Comparator;

import com.gft.practica.models.Apple;

public class Capitulo3 {

	
	//Sin lambdas
	
	Comparator<Apple> byWeight=new Comparator<Apple>() {
		
		public int compare(Apple a1, Apple a2) {
			return a1.getWeight().compareTo(a2.getWeight());
			
		}
		
		
	};
	
	//con lambdas
	
	Comparator<Apple>byWeightLambda=(Apple a1,Apple a2)->a1.getWeight().compareTo(a2.getWeight());
	
	
	
	
	
	
}
