package com.gft.practica.capitulos.cap3;

public interface Predicate <T>{

	
	boolean test(T t);
	
}
