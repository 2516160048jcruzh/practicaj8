package com.gft.practica.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Apple {

	
	String color;
	Integer weight;
}
